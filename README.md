# Angular-Python-Recommender-System

About this course
Develop and launch your own fully working Recommender System

Course content

###### Section 01: Introduction
###### Section 02: Angular Website
###### Section 03: Mysql
###### Section 04: Python Backend Recommender
###### Section 05: Application Styling (Optional)
###### Section 06: Authentication (Firebase)
###### Section 07: Update Backend
###### Section 08: Save user rating through web app
from flask import Flask
import json
from Recommender import getRecommendedItems


app = Flask(__name__)
@app.route("/predictions/<int:uid>", strict_slashes=False)

def predictions(uid):
    return json.dumps(getRecommendedItems(uid), indent=2)


if __name__== '__main__':
    app.run(debug=True)

'''
C:\Users\Jarkko\Desktop\Udemy\backendRecommender\venv\Scripts\python.exe C:/Users/Jarkko/Desktop/Udemy/backendRecommender/app.py
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 300-569-542
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 
 should runnig http://127.0.0.1:5000/predictions/1 to 5

'''